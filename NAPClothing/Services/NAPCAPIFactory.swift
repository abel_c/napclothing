//
//  NAPCAPIFactory.swift
//  NAPClothing
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

class NAPCAPIFactory {
  
  func getAPI() -> NAPCAPIProtocol {
    
    #if OFFLINE
      return NAPCOfflineAPI()
    #else
      return NAPCAPI()
    #endif
  }
}
