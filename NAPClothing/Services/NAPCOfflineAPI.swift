//
//  NAPCOfflineAPI.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPNetwork
import NAPCore

class NAPCOfflineAPI : NAPCAPIProtocol {
  var error: NSError?
  var responseToRet: NAPResponseProtocol?
  var offlineProvider = NAPCOfflineProvider()
  
  func getSummaries(completionHandler: @escaping (Result<NAPCSummaryResponse>) -> ()) {
    let response: Result<NAPCSummaryResponse>
    if let error = error {
      response = .failure(error: error)
    }
    else {
      response = .success(value: (responseToRet as? NAPCSummaryResponse) ?? offlineProvider.summaryResponse)
    }
    completionHandler(response)
  }
}
