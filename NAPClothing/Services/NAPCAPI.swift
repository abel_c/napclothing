//
//  NAPCAPI.swift
//  NAPClothing
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPNetwork

protocol NAPCAPIProtocol {
  func getSummaries(completionHandler: @escaping (Result<NAPCSummaryResponse>) -> ())
}

class NAPCAPI : NAPCAPIProtocol {
  
  fileprivate struct ServicePaths {
    static let summary = "summaries"
  }
  
  func getSummaries(completionHandler: @escaping (Result<NAPCSummaryResponse>) -> ()) {
    NAPNetworkManager.sharedInstance.request(path: ServicePaths.summary, params: NAPCSummaryRequest().params, completionHandler: completionHandler)
  }
  
}
