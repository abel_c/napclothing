//
//  NAPCSummaryRequest.swift
//  NAPClothing
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

struct NAPCSummaryRequest {
  
  let params: [String:Any] = ["categoryIds":2]
  
}
