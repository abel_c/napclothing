//
//  NAPCSummaryResponse.swift
//  NAPClothing
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPNetwork

class NAPCSummaryResponse : NAPResponseProtocol {
  
  var summary: NAPCSummaryProtocol?
  
  static func responseObject(data: [String : Any]) -> Self {
    return self.init(data: data)
  }
  
  required init(data: [String : Any]) {
    summary = NAPCSummary(dictionary: data)
  }
  
  // Testing
  init(summary: NAPCSummaryProtocol) {
    self.summary = summary
  }
  
}
