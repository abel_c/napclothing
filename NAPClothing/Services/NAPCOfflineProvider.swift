//
//  NAPCOfflineProvider.swift
//  NAPClothing
//
//  Created by Abel Castro on 13/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPCore

struct NAPCOfflineProvider {
  var summaryResponse: NAPCSummaryResponse {
    let image = NAPCImage(shots: Set<NAPCImageShot>([NAPCImageShot._in]), sizes: Set<NAPCImageSize>([NAPCImageSize.m]), mediaType: "jpeg", urlTemplate: "{{scheme}}//cache.net-a-porter.com/images/products/942725/942725_{{shot}}_{{size}}.jpg")
    let price = NAPCPrice(currency: "GBP", divisor: 100, amount: 40000)
    
    var items: [NAPCItemProtocol] = [NAPCItem]()
    for index in 0..<100 {
      let item = NAPCItem(name: "item\(index)", visible: true, saleableStandardSizeIds: Set<NAPCItemSizeId>([NAPCItemSizeId.m]), price: price, leafCategoryIds: [1], onSale: false, analyticsKey: "item\(index)", id: index, brandId: 1, colourIds: [1], images: image, badges: Set<NAPCBadge>([NAPCBadge.Exclusive]))
      items.append(item)
    }
    
    
    let listInfo = NAPCListInfo(limit: 10, offset: 0, total: 30000)
    let summary = NAPCSummary(items: items, listInfo: listInfo)
    return NAPCSummaryResponse(summary: summary)
  }
  
}
