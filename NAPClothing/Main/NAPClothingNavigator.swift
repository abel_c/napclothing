//
//  NAPClothingNavigator.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

protocol NAPClothingNavigatorProtocol : class {
  func toHome()
}

class NAPClothingNavigator : NAPClothingNavigatorProtocol {
  
  let navigationController: UINavigationController
  
  let presenterFactory: NAPClothingPresenterFactoryProtocol
  let viewControllerFactory: NAPClothingViewControllerFactoryProtocol
  
  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    presenterFactory = NAPClothingPresenterFactory()
    viewControllerFactory = NAPClothingViewControllerFactory(presenterFactory: presenterFactory)
  }
  
  func toHome() {
    let homeVC = viewControllerFactory.getHomeViewController(navigator: self)
    var vcs = self.navigationController.viewControllers
    vcs.append(homeVC)
    self.navigationController.setViewControllers(vcs, animated: true)
  }

}
