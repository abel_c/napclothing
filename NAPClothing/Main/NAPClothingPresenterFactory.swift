//
//  NAPClothingPresenterFactory.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

protocol NAPClothingPresenterFactoryProtocol {
  func getHomePresenter(navigator: NAPClothingNavigatorProtocol) -> NAPCHomePresenterProtocol
}

class NAPClothingPresenterFactory : NAPClothingPresenterFactoryProtocol {
  func getHomePresenter(navigator: NAPClothingNavigatorProtocol) -> NAPCHomePresenterProtocol {
    return NAPCHomePresenter()
  }
}

