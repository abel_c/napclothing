//
//  NAPClothingViewControllerFactory.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

protocol NAPClothingViewControllerFactoryProtocol {
  func getHomeViewController(navigator: NAPClothingNavigatorProtocol) -> UIViewController
}

class NAPClothingViewControllerFactory : NAPClothingViewControllerFactoryProtocol {
  
  let presenterFactory: NAPClothingPresenterFactoryProtocol
  
  init(presenterFactory: NAPClothingPresenterFactoryProtocol) {
    self.presenterFactory = presenterFactory
  }
  
  func getHomeViewController(navigator: NAPClothingNavigatorProtocol) -> UIViewController {
    return NAPCHomeViewController(presenter: presenterFactory.getHomePresenter(navigator: navigator))
  }
}
