//
//  NAPClothingManager.swift
//  NAPClothing
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

public class NAPClothingManager {
  
  let navigator: NAPClothingNavigatorProtocol
  
  public init(navigationController: UINavigationController) {
    navigator = NAPClothingNavigator(navigationController: navigationController)
  }
  public func start() {    
    navigator.toHome()
  }
}
