//
//  NAPCHomePresenter.swift
//  NAPClothing
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPNetwork

protocol NAPCHomeViewProtocol : class {
  func set(hidesBackButton: Bool)
  func showSummary()
  func show(error: Error)
  func showAlert(message: String, positive: Bool)
}

protocol NAPCHomePresenterProtocol : class {
  weak var managedView: NAPCHomeViewProtocol? {get set}
  var summary: NAPCSummaryProtocol? {get}
  
  func start()
}


class NAPCHomePresenter : NAPCHomePresenterProtocol {
  weak var managedView: NAPCHomeViewProtocol?
  var summary: NAPCSummaryProtocol?
  var api: NAPCAPIProtocol
  init(api: NAPCAPIProtocol = NAPCAPIFactory().getAPI()) {
    self.api = api
    // Makes use of Observer pattern, to get notifications
    NAPReachability.sharedInstance.add(listener: self)
  }
  
  func start() {
    self.managedView?.set(hidesBackButton: true)

    self.api.getSummaries { [weak self] (result) in
      switch result {
      case .success(let response):
        self?.summary = response.summary
        self?.managedView?.showSummary()
      case .failure(let error):
        self?.managedView?.show(error: error)
      }
    }
  }
}

// Extends the behaviour of the class, thanks to Observer pattern
extension NAPCHomePresenter : NAPReachabilityListenerProtocol {
  func notify(message: String, positive: Bool) {
    self.managedView?.showAlert(message: message, positive: positive)
  }
}
