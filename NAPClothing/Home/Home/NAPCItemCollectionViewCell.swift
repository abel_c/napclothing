//
//  NAPCItemCollectionViewCell.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import UIKit

class NAPCItemCollectionViewCell: UICollectionViewCell {
  
  static let identifier = NAPCItemCollectionViewCell.self
  static let cellNib = UINib(nibName: String(describing: identifier), bundle: Bundle(for: identifier))
  static let size: CGSize = {
    let width = (UIScreen.main.bounds.size.width - 40) / 2
    let height = 1.5 * width
    return CGSize(width: width, height: height)
  }()
  
  @IBOutlet fileprivate weak var grayView: UIView!
  @IBOutlet fileprivate weak var nameLabel: UILabel!
  @IBOutlet fileprivate weak var imageView: UIImageView!
  @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet fileprivate weak var priceLabel: UILabel!
  @IBOutlet fileprivate weak var favouriteButton: UIButton!
  
  fileprivate var presenter: NAPCItemCellPresenterProtocol!
  override func awakeFromNib() {
    super.awakeFromNib()
    grayView.layer.cornerRadius = 4
  }
  override func prepareForReuse() {
    nameLabel.text = nil
    imageView.image = nil
  }

  @IBAction func favouriteButtonTouchUpInside(_ sender: Any) {
    self.presenter.favouriteButtonTouchUpInside()
  }
}

extension NAPCItemCollectionViewCell : NAPCItemCellViewProtocol {
  func attach(presenter: NAPCItemCellPresenterProtocol) {
    self.presenter = presenter
    self.presenter.cell = self
  }
  func startLoading() {
    activityIndicator.startAnimating()
  }
  func stopLoading() {
    activityIndicator.stopAnimating()
  }
  func set(name: String) {
    nameLabel.text = name
  }
  func set(price: String) {
    priceLabel.text = price
  }
  func set(image: UIImage?) {
    imageView.image = image
  }
  func set(favouriteButtonImage: UIImage?) {
    favouriteButton.setImage(favouriteButtonImage, for: .normal)
  }
}
