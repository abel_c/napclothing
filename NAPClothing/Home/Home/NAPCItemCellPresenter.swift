//
//  NAPCItemCellPresenter.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPNetwork
import NAPCore

protocol NAPCItemCellViewProtocol : class {
  func attach(presenter: NAPCItemCellPresenterProtocol)
  func startLoading()
  func stopLoading()
  func set(name: String)
  func set(price: String)
  func set(image: UIImage?)
  func set(favouriteButtonImage: UIImage?)
}
protocol NAPCItemCellPresenterProtocol {
  weak var cell: NAPCItemCellViewProtocol? {get set}
  func favouriteButtonTouchUpInside()
}

struct NAPCItemCellPresenter : NAPCItemCellPresenterProtocol {
  let imageCacheManager: NAPImageCacheManagerProtocol
  let persistItemManager: NAPPersistItemManagerProtocol
  
  weak var cell: NAPCItemCellViewProtocol?
  var item: NAPCItemProtocol!
  
  let numberFormatter: NumberFormatter = {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .currency
    numberFormatter.minimumFractionDigits = 2
    numberFormatter.maximumFractionDigits = 2
    return numberFormatter
  }()
  
  init(imageCacheManager: NAPImageCacheManagerProtocol = NAPImageCacheManager(), persistItemManager: NAPPersistItemManagerProtocol = NAPPersistItemManager.sharedInstance) {
    self.imageCacheManager = imageCacheManager
    self.persistItemManager = persistItemManager
  }
  mutating func attatch(cell: NAPCItemCellViewProtocol, item: NAPCItemProtocol ) {
    self.cell = cell
    self.item = item
    cell.attach(presenter: self)
  }
  
  func configure(cell: NAPCItemCellViewProtocol) {
    
    cell.set(name: item.name)
  
    numberFormatter.currencyCode = item.price.currency
    cell.set(price: numberFormatter.string(from: NSDecimalNumber(value: item.price.amount / item.price.divisor)) ?? NSLocalizedString("N/A", comment: "not available price"))
    
    cell.startLoading()
    self.imageCacheManager.getImage(for: item) { (image) in
      DispatchQueue.main.async {
        cell.set(favouriteButtonImage: self.getButtonImage(favourite: self.persistItemManager.isFavourite(item: self.item)))
        cell.stopLoading()
        cell.set(image: image ?? UIImage(named: "Image-not-available", in: Bundle(for: NAPCItemCollectionViewCell.self), compatibleWith: nil))
      }
    }
  }
  
  func favouriteButtonTouchUpInside() {
    self.cell?.set(favouriteButtonImage: getButtonImage(favourite: !self.persistItemManager.isFavourite(item: self.item)))

    if self.persistItemManager.isFavourite(item: self.item) {
      self.persistItemManager.removeFavourite(item: item)
    }
    else {
      self.persistItemManager.addFavourite(item: item)
    }
    
  }
  
  fileprivate func getButtonImage(favourite: Bool) -> UIImage? {
    return UIImage(named: favourite ? "heart2" : "heart1", in: Bundle(for: NAPCItemCollectionViewCell.self), compatibleWith: nil)

  }

}
