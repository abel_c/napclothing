//
//  NAPCHomeViewController.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import UIKit

class NAPCHomeViewController: UIViewController {
  
  let identifier = NAPCHomeViewController.self
  
  var cellPresenter = NAPCItemCellPresenter()
  let presenter: NAPCHomePresenterProtocol
  
  init(presenter: NAPCHomePresenterProtocol) {
    self.presenter = presenter
    super.init(nibName: String(describing: identifier), bundle: Bundle(for: identifier))
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  @IBOutlet fileprivate weak var alertMessage: UILabel!
  @IBOutlet fileprivate weak var alertView: UIView!
  @IBOutlet fileprivate weak var itemsCollectionView: UICollectionView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    presenter.managedView = self
    itemsCollectionView.register(NAPCItemCollectionViewCell.cellNib, forCellWithReuseIdentifier: String(describing: NAPCItemCollectionViewCell.identifier))
    itemsCollectionView.dataSource = self
    itemsCollectionView.delegate = self
    itemsCollectionView.contentInset = UIEdgeInsets(top: 32, left: 16, bottom: 32, right: 16)
    
    presenter.start()
  }
  
}

// MARK: - UICollectionViewDataSource
extension NAPCHomeViewController : UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return presenter.summary?.items.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: UICollectionViewCell
    if let dequeuedCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NAPCItemCollectionViewCell.identifier), for: indexPath) as? NAPCItemCollectionViewCell, let summary = presenter.summary {
      cellPresenter.attatch(cell: dequeuedCell, item: summary.items[indexPath.row])
      cellPresenter.configure(cell: dequeuedCell)
      cell = dequeuedCell
    }
    else {
      cell = UICollectionViewCell()
    }
    return cell
  }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension NAPCHomeViewController : UICollectionViewDelegateFlowLayout {  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return NAPCItemCollectionViewCell.size
  }
}

extension NAPCHomeViewController : NAPCHomeViewProtocol {
  func set(hidesBackButton: Bool) {
    navigationItem.hidesBackButton = hidesBackButton
  }
  func showSummary() {
    itemsCollectionView.reloadData()
  }
  func show(error: Error) {
    itemsCollectionView.showEmpty(message: error.localizedDescription)
  }
  func showAlert(message: String, positive: Bool) {
    self.alertView.backgroundColor = positive ? .green : .red
    self.alertMessage.text = message
    self.alertView.isHidden = false
    UIView.animate(withDuration: 0.4, animations: { [weak self] in
      self?.view.layoutIfNeeded()
    }) { (_) in
      if positive {
        UIView.animate(withDuration: 0.4, delay: 2.0, options: [], animations: { [weak self] in
          self?.alertView.isHidden = true
          self?.view.layoutIfNeeded()
        }, completion: nil)
      }
    }
  }

}
