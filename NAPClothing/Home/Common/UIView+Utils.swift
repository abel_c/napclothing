//
//  UIView+Utils.swift
//  NAPClothing
//
//  Created by Abel Castro on 13/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

extension UICollectionView {
  func showEmpty(message: String) {
    let label = UILabel(frame: CGRect.zero)
    label.textAlignment = .center
    label.text = message
    label.numberOfLines = 0
    backgroundView = label
  }
}
