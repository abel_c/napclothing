//
//  NAPCSummary.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPCore

protocol NAPCSummaryProtocol {
  var items: [NAPCItemProtocol] {get}
  var listInfo: NAPCListInfoProtocol {get}
}
struct NAPCSummary : NAPCSummaryProtocol {
  fileprivate struct Keys {
    static let summaries = "summaries"
    static let listInfo = "listInfo"
  }
  var items: [NAPCItemProtocol]
  var listInfo: NAPCListInfoProtocol
  
  init?(dictionary: [String:Any]) {
    if  let summariesArray = dictionary[Keys.summaries] as? [[String:Any]],
        let listInfoDict = dictionary[Keys.listInfo] as? [String:Any],
        let listInfo = NAPCListInfo(dictionary: listInfoDict) {
      self.items = NAPCSummary.createArrayOfItems(array: summariesArray)
      self.listInfo = listInfo
    }
    else {
      return nil
    }
  }
  
  fileprivate static func createArrayOfItems(array: [[String:Any]]) -> [NAPCItemProtocol] {
    var items = [NAPCItemProtocol]()
    array.forEach { (dict) in
      if let item = NAPCItem(dictionary: dict) {
        items.append(item)
      }
    }
    return items
  }
  
  // Testing
  init(items: [NAPCItemProtocol], listInfo: NAPCListInfoProtocol) {
    self.items = items
    self.listInfo = listInfo
  }
}
