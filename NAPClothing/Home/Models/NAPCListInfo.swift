//
//  ListInfo.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

protocol NAPCListInfoProtocol {
  var limit: Int {get}
  var offset: Int {get}
  var total: Double {get}
}
struct NAPCListInfo : NAPCListInfoProtocol {
  fileprivate struct Keys {
    static let limit = "limit"
    static let offset = "offset"
    static let total  = "total"
  }
  
  var limit: Int
  var offset: Int
  var total: Double
  
  init?(dictionary: [String:Any]) {
    if  let limit = dictionary[Keys.limit] as? Int,
      let offset = dictionary[Keys.offset] as? Int,
      let total = dictionary[Keys.total] as? Double {
      self.limit = limit
      self.offset = offset
      self.total = total
    }
    else {
      return nil
    }
  }
  
  // Testing
  init(limit: Int, offset: Int, total: Double) {
    self.limit = limit
    self.offset = offset
    self.total = total
  }
}
