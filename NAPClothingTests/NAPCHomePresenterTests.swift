//
//  NAPCHomePresenterTests.swift
//  NAPClothingTests
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
@testable import NAPClothing

class NAPCHomePresenterTests: XCTestCase {
  
  fileprivate var apiMock: NAPCAPIProtocol!
  fileprivate var viewMock: HomeViewMock!
  fileprivate var presenter: NAPCHomePresenter!
  
  override func setUp() {
    super.setUp()
    apiMock = NAPCAPIMock()
    viewMock = HomeViewMock()
    presenter = NAPCHomePresenter(api: apiMock)
    presenter.managedView = viewMock
  }

  func testSuccess() {
    // Given
    XCTAssertFalse(viewMock.summaryShown)
    (apiMock as? NAPCAPIMock)?.expectation = expectation(description: "get_summary")
    
    // When
    presenter.start()
    
    // Then
    XCTAssertTrue(viewMock.backButtonHidden, "Back button should be hidden")
    
    waitForExpectations(timeout: 5.0) { _ in
      XCTAssertTrue(self.viewMock.summaryShown, "Summary should be shown")
      XCTAssertNil(self.viewMock.error, "Error should be nil")
      XCTAssertNotNil(self.presenter.summary, "Summmary should not be nil")
    }
  }
  
  func testFailure() {
    // Given
    XCTAssertFalse(viewMock.summaryShown)
    (apiMock as? NAPCAPIMock)?.expectation = expectation(description: "get_summary")
    let errorMessage = "The Internet connection appears to be offline."
    let userInfo: [String: Any] = [NSLocalizedDescriptionKey : errorMessage]
    (apiMock as? NAPCAPIMock)?.error = NSError(domain: String(describing: NAPCHomePresenterTests.self), code: 404, userInfo: userInfo)
    
    // When
    presenter.start()
    
    // Then
    XCTAssertTrue(viewMock.backButtonHidden, "Back button should be hidden")
    
    waitForExpectations(timeout: 5.0) { _ in
      XCTAssertFalse(self.viewMock.summaryShown, "Summary should not be shown")
      XCTAssertNotNil(self.viewMock.error, "Error should not be nil")
      XCTAssertEqual(self.viewMock.error?.localizedDescription, errorMessage)
      XCTAssertNil(self.presenter.summary, "Summmary should be nil")
    }
  }
  
  func testMalformedResponse() {
    // Given
    XCTAssertFalse(viewMock.summaryShown)
    (apiMock as? NAPCAPIMock)?.expectation = expectation(description: "get_summary")
    
    
    (apiMock as? NAPCAPIMock)?.responseToRet = NAPCSummaryResponse(data: [:])
    
    // When
    presenter.start()
    
    // Then
    XCTAssertTrue(viewMock.backButtonHidden, "Back button should be hidden")
    
    waitForExpectations(timeout: 5.0) { _ in
      XCTAssertNil(self.presenter.summary, "Summmary should be nil")
    }
  }
  
  func test_notifyMessage_positive() {
    // When
    presenter.notify(message: "Message", positive: true)
    
    // Then
    XCTAssertEqual(viewMock.message, "Message")
    XCTAssertTrue(viewMock.positive)
  }
  
  func test_notifyMessage_negative() {
    // When
    presenter.notify(message: "Message", positive: false)
    
    // Then
    XCTAssertEqual(viewMock.message, "Message")
    XCTAssertFalse(viewMock.positive)
  }
}

fileprivate class HomeViewMock : NAPCHomeViewProtocol {
  var backButtonHidden: Bool = false
  var summaryShown: Bool = false
  var error: Error?

  var message: String = ""
  var positive: Bool = false
  init() {}
  func set(hidesBackButton: Bool) {
    self.backButtonHidden = hidesBackButton
  }
  func showSummary() {
    summaryShown = true
  }
  func show(error: Error) {
    self.error = error
  }
  func showAlert(message: String, positive: Bool) {
    self.message = message
    self.positive = positive
  }

}
