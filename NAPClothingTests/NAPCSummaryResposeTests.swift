//
//  NAPCSummaryResposeTests.swift
//  NAPClothingTests
//
//  Created by Abel Castro on 14/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
import NAPCore
@testable import NAPClothing

class NAPCSummaryResposeTests: XCTestCase {
  
  var response: NAPCSummaryResponse!
  
  func testParsing() {
    // Given
    let item1: [String:Any] = ["name":"Isadora oversized striped cotton-blend shirt","visible":true,"saleableStandardSizeIds":["00002_XXS_Clothing","00003_XS_Clothing","00004_S_Clothing","00005_M_Clothing","00006_L_Clothing","00007_XL_Clothing","00008_XXL_Clothing"],"price":["currency":"GBP","divisor":100,"amount":17000],"leafCategoryIds":[6229,6070,2855],"onSale":false,"analyticsKey":"Isadora oversized striped cotton-blend shirt","id":991830,"brandId":312,"colourIds":[3],"images":["shots":["in","ou","fr","bk","cu"],"sizes":["dl","l","m","m2","mt","mt2","pp","s","sl","xl","xs","xxl"],"mediaType":"image/jpeg","urlTemplate":"{{scheme}}//cache.net-a-porter.com/images/products/991830/991830_{{shot}}_{{size}}.jpg"],"badges":["Exclusive","In_Stock"]]
    
    let listInfo: [String : Any]  = ["limit":5,"offset":0,"total": Double(11709)]
    let summaries: [[String : Any]] = [item1]
    let data: [String:Any] = ["summaries": summaries, "listInfo" : listInfo]
    
    // When
    response = NAPCSummaryResponse(data: data)
    
    // Then
    XCTAssertNotNil(response.summary)
    guard let summary = response.summary else { return }
    XCTAssertEqual(summary.listInfo.limit, 5)
    XCTAssertEqual(summary.listInfo.offset, 0)
    XCTAssertEqual(summary.listInfo.total, 11709)
    
    XCTAssertEqual(summary.items.count, 1)

    if let item = summary.items.first {
      XCTAssertEqual(item.name, "Isadora oversized striped cotton-blend shirt")
      XCTAssertTrue(item.visible)
      XCTAssertEqual(item.saleableStandardSizeIds, Set<NAPCItemSizeId>([.xxs, .xs, .s, .m, .l, .xl, .xxl]))
      XCTAssertEqual(item.price.currency, "GBP")
      XCTAssertEqual(item.price.divisor, 100)
      XCTAssertEqual(item.price.amount, 17000)
      XCTAssertEqual(item.leafCategoryIds, [6229, 6070, 2855])
      XCTAssertFalse(item.onSale)
      XCTAssertEqual(item.analyticsKey, "Isadora oversized striped cotton-blend shirt")
      XCTAssertEqual(item.id, 991830)
      XCTAssertEqual(item.brandId, 312)
      XCTAssertEqual(item.colourIds, [3])
      XCTAssertEqual(item.images.shots, Set<NAPCImageShot>([._in, .ou, .fr, .bk, .cu]))
      XCTAssertEqual(item.images.sizes, Set<NAPCImageSize>([.dl, .l , .m, .m2, .mt, .mt2, .pp, .s, .sl, .xl, .xs, .xxl]))
      XCTAssertEqual(item.images.mediaType, "image/jpeg")
      XCTAssertEqual(item.images.urlTemplate, "{{scheme}}//cache.net-a-porter.com/images/products/991830/991830_{{shot}}_{{size}}.jpg")
      XCTAssertEqual(item.badges, Set<NAPCBadge>([.In_Stock, .Exclusive]))
    }
  }
  
  func testParsingMalformed() {
    // Given
    let listInfo: [String : Any]  = ["limit":5,"offset":0]
    let summaries: [[String : Any]] = []
    let data: [String:Any] = ["summaries": summaries, "listInfo" : listInfo]
    
    // When
    response = NAPCSummaryResponse(data: data)
    
    // Then
    XCTAssertNil(response.summary)    
  }

}

