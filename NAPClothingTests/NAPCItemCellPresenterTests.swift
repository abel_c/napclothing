//
//  NAPCItemCellPresenterTests.swift
//  NAPClothingTests
//
//  Created by Abel Castro on 13/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
import NAPNetwork
import NAPCore
@testable import NAPClothing

class NAPCItemCellPresenterTests: XCTestCase {
  fileprivate var presenter: NAPCItemCellPresenter!
  fileprivate var imageCacheManagerMock: NAPImageCacheManagerMock!
  fileprivate var persistItemManagerMock: NAPPersistItemManagerMock!
  fileprivate var cellMock: CellMock!
  
  override func setUp() {
    super.setUp()
    imageCacheManagerMock = NAPImageCacheManagerMock()
    persistItemManagerMock = NAPPersistItemManagerMock()
    cellMock = CellMock()
    presenter = NAPCItemCellPresenter(imageCacheManager: imageCacheManagerMock, persistItemManager: persistItemManagerMock)    
  }
  
  func testAttach() {
    // Given
    let item: NAPCItemProtocol = NAPCItem.get()
    
    // When
    presenter.attatch(cell: cellMock, item: item)
    
    // Then
    XCTAssertTrue(cellMock.attached, "Presenter should be attatched")
  }
  
  func testConfigure() {
    // Given
    imageCacheManagerMock.expectation = expectation(description: "get_image")
    let item: NAPCItemProtocol = NAPCItem.get()
    presenter.attatch(cell: cellMock, item: item)
    
    // When
    presenter.configure(cell: cellMock)
    
    // Then
    XCTAssertTrue(cellMock.loading, "Item should be loading")
    XCTAssertEqual(cellMock.name, item.name, "Item name should be the same")
    XCTAssertEqual(cellMock.name, "name", "Item name should be the same")
    XCTAssertEqual(cellMock.price, "£400.00", "Item price should be the same")

    waitForExpectations(timeout: 5.0) { _ in
      XCTAssertFalse(self.cellMock.loading, "Item should not be loading")
      XCTAssertNotNil(self.cellMock.image, "Image should not be nil")
      XCTAssertNotNil(self.cellMock.favouriteButtonImage, "Button image should not be nil")
    }
  }
  
  func testConfigureNoImage() {
    // Given
    imageCacheManagerMock.expectation = expectation(description: "get_image")
    imageCacheManagerMock.image = nil
    let item: NAPCItemProtocol = NAPCItem.get()
    presenter.attatch(cell: cellMock, item: item)
    
    // When
    presenter.configure(cell: cellMock)
    
    // Then
    waitForExpectations(timeout: 5.0) { _ in
      XCTAssertFalse(self.cellMock.loading, "Item should not be loading")
      XCTAssertNotNil(self.cellMock.image, "Image should not be nil")
      XCTAssertEqual(UIImagePNGRepresentation(self.cellMock.image!), UIImagePNGRepresentation(UIImage(named: "Image-not-available", in: Bundle(for: NAPCItemCollectionViewCell.self), compatibleWith: nil)!))
      XCTAssertNotNil(self.cellMock.favouriteButtonImage, "Button image should not be nil")
    }
  }
  
  func testFavouriteTouchUpInside_notFavourite() {
    // Given
    imageCacheManagerMock.expectation = expectation(description: "get_image")
    persistItemManagerMock.favourite = false
    let item: NAPCItemProtocol = NAPCItem.get()
    presenter.attatch(cell: cellMock, item: item)
    presenter.configure(cell: cellMock)

    let favouriteImage = UIImagePNGRepresentation(UIImage(named: "heart2", in: Bundle(for: NAPCItemCollectionViewCell.self), compatibleWith: nil)!)
    let notFavouriteImage = UIImagePNGRepresentation(UIImage(named: "heart1", in: Bundle(for: NAPCItemCollectionViewCell.self), compatibleWith: nil)!)
    
    waitForExpectations(timeout: 5.0) { _ in

      XCTAssertEqual(UIImagePNGRepresentation(self.cellMock.favouriteButtonImage!), notFavouriteImage)

      // When
      self.presenter.favouriteButtonTouchUpInside()
      
      // Then
      XCTAssertEqual(UIImagePNGRepresentation(self.cellMock.favouriteButtonImage!), favouriteImage)
    }
  }
  
  func testFavouriteTouchUpInside_favourite() {
    // Given
    imageCacheManagerMock.expectation = expectation(description: "get_image")
    let item: NAPCItemProtocol = NAPCItem.get()
    presenter.attatch(cell: cellMock, item: item)
    presenter.configure(cell: cellMock)
    
    let favouriteImage = UIImagePNGRepresentation(UIImage(named: "heart2", in: Bundle(for: NAPCItemCollectionViewCell.self), compatibleWith: nil)!)
    let notFavouriteImage = UIImagePNGRepresentation(UIImage(named: "heart1", in: Bundle(for: NAPCItemCollectionViewCell.self), compatibleWith: nil)!)
    
    waitForExpectations(timeout: 5.0) { _ in
      
      XCTAssertEqual(UIImagePNGRepresentation(self.cellMock.favouriteButtonImage!), favouriteImage)
      
      // When
      self.presenter.favouriteButtonTouchUpInside()
      
      // Then
      XCTAssertEqual(UIImagePNGRepresentation(self.cellMock.favouriteButtonImage!), notFavouriteImage)
    }
  }
}

fileprivate class CellMock : NAPCItemCellViewProtocol {
  var attached: Bool = false
  var loading: Bool = false
  var name: String = ""
  var price: String = ""
  var image: UIImage?
  var favouriteButtonImage: UIImage?
  init() {}
  
  func attach(presenter: NAPCItemCellPresenterProtocol) {
    attached = true
  }
  
  func startLoading() {
    loading = true
  }
  
  func stopLoading() {
    loading = false
  }
  
  func set(name: String) {
    self.name = name
  }
  
  func set(price: String) {
    self.price = price
  }
  
  func set(image: UIImage?) {
    self.image = image
  }
  
  func set(favouriteButtonImage: UIImage?) {
    self.favouriteButtonImage = favouriteButtonImage
  }
  
  
}
