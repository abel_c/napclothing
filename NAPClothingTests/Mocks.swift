//
//  Mocks.swift
//  NAPClothingTests
//
//  Created by Abel Castro on 13/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import XCTest
import NAPNetwork
import NAPCore

@testable import NAPClothing

class NAPCAPIMock : NAPCOfflineAPI {
  var expectation: XCTestExpectation?
  
  override func getSummaries(completionHandler: @escaping (Result<NAPCSummaryResponse>) -> ()) {
    super.getSummaries(completionHandler: completionHandler)
    expectation?.fulfill()
  }

}

class NAPImageCacheManagerMock : NAPImageCacheManagerProtocol {
  var image: UIImage? = UIImage()
  var expectation: XCTestExpectation?
  var flushedCache: Bool = false
  init() {}
  func getImage(for item: NAPCItemProtocol, completionHandler: @escaping (UIImage?) ->()) {
    completionHandler(image)
    // wait short time to complete
    DispatchQueue.global().asyncAfter(deadline: .now() + 0.3) { [weak self] in
      self?.expectation?.fulfill()
    }
  }
  func flushCache() {
    flushedCache = true
  }

}

class NAPPersistItemManagerMock : NAPPersistItemManagerProtocol {
  var favourite: Bool = true
  
  init() {}
  static var sharedInstance: NAPPersistItemManagerProtocol = NAPPersistItemManagerMock()
  func removeFavourite(item: NAPCItemProtocol) {}
  func addFavourite(item: NAPCItemProtocol) {}
  func isFavourite(item: NAPCItemProtocol) -> Bool {
    return favourite
  }
  func saveFavourites() {}
}

class NAPClothingPresenterFactoryMock : NAPClothingPresenterFactoryProtocol {
  var gotHomePresenter: Bool = false
  init() {}
  func getHomePresenter(navigator: NAPClothingNavigatorProtocol) -> NAPCHomePresenterProtocol {
    gotHomePresenter = true
    let presenter: NAPCHomePresenterProtocol = {
      class Presenter : NAPCHomePresenterProtocol {
        weak var managedView: NAPCHomeViewProtocol?
        var summary: NAPCSummaryProtocol?
        func start() {}
      }
      return Presenter()
    }()
    return presenter
  }
}

class NAPClothingNavigatorMock : NAPClothingNavigatorProtocol {
  var toHomed: Bool = false
  func toHome() {
    toHomed = true
  }
}
