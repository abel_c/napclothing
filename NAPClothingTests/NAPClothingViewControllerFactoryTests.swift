//
//  NAPClothingViewControllerFactoryTests.swift
//  NAPClothingTests
//
//  Created by Abel Castro on 14/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
@testable import NAPClothing

class NAPClothingViewControllerFactoryTests: XCTestCase {
  
  fileprivate var factory: NAPClothingViewControllerFactoryProtocol!
  fileprivate var presenterFactory = NAPClothingPresenterFactoryMock()
  override func setUp() {
    super.setUp()
    factory = NAPClothingViewControllerFactory(presenterFactory: presenterFactory)
  }

  func test_getHomeViewController() {
    // Given
    let navigator = NAPClothingNavigatorMock()
    XCTAssertFalse(presenterFactory.gotHomePresenter, "Home presenter should not called yet")
    // When
    _ = factory.getHomeViewController(navigator: navigator)
    // Then
    XCTAssertTrue(presenterFactory.gotHomePresenter, "Home presenter should have been called")
  }
}

