//
//  NAPClothingNavigatorTests.swift
//  NAPClothingTests
//
//  Created by Abel Castro on 14/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
@testable import NAPClothing

class NAPClothingNavigatorTests: XCTestCase {
  
  fileprivate var navigationController = UINavigationControllerExtended()
  fileprivate var navigator: NAPClothingNavigatorProtocol!
  
  override func setUp() {
    super.setUp()
    navigator = NAPClothingNavigator(navigationController: navigationController)
  }
  func test_toHome() {
    // When
    navigator.toHome()
    // Then
    XCTAssertTrue(navigationController.viewControllersSet, "View controllers should have been set")
  }
}

fileprivate class UINavigationControllerExtended : UINavigationController {
  var viewControllersSet = false
  override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
    viewControllersSet = true
    super.setViewControllers(viewControllers, animated: animated)
  }
}
